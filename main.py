import forecastio
from time import sleep
import os
from datetime import datetime
from geopy.geocoders import Nominatim
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

day = []
config = ConfigParser()
#Get your API key here: https://darksky.net/dev/
key= "API_KEY"
geolocator = Nominatim()
ini = input("Do you want to use the ini file as input method? (Yes/no)" )
while  True:
    if ini == "no" or ini == "No":
         address = input("Enter your City/Zip code/Address: ")
         interval = int(input("Enter the interval: "))
         target = input("Enter the target temperature (Celsius): ")
         how_long = input("Enter how long you want the program to run (Enter 0 zero for unlimited checking): ")
    elif ini == "yes" or ini == "Yes":
         config.read('weather.ini')
         address = config.get('Start','address')
         interval = config.getint('Start','interval')
         target = config.getint('Start','target')
         how_long = config.getint('Start','how_long')
         break
    else:
	    ini = input("Enter yes or no: ")
	    continue

loc = geolocator.geocode(address)
lat = loc.latitude
lng =loc.longitude
print("DEBUG")
print(lat)
print(lng)
print("DEBUG")
t = 0 
w = open("temp.txt",'a')
counter = 0
while True:
    forecast = forecastio.load_forecast(key,lat,lng).currently()
    temp = forecast.temperature
    time = datetime.now().time()
    print("It is ",temp," degrees (",time,")")
    sav = str(temp)," : ",str(time)
    day +=sav
    w.write(''.join(sav))
    if counter > 0:
            w.write(''.join(sav))
    if (temp>= int(target)):
        print("Time for meditation!")
        os.system("start _Trommel.wav")
        target = input("Set a new target temperature: ")
        pass
    sleep(interval * 60)
    counter = counter + 1
    if counter == 0:
    	t += 0
    else:
    	t += interval * 60
    if int(how_long == 0):
        pass
    if int(how_long) >= t and int(how_long) != 0:
        print("Done at ", datetime.now().time())
        
